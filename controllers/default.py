# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Welcome to web2py!")
    return dict(message=T('Hello World'))


@auth.requires_login()
def cadastro():
    if request.args(0):
        form = SQLFORM(db.evento, request.args(0), submit_button='Cadastrar Evento')
    else:
        form = SQLFORM(db.evento, submit_button='Cadastrar Evento')

    if form.process().accepted:
        response.flash = 'Dados cadastrados'
    elif form.errors:
        response.flash = 'Foram encontrados erros. Favor verificar os campos abaixo'
    else:
        response.flash = 'Favor preencher o formulário'

    return locals()

@auth.requires_login()
def meuseventos():
    form = SQLFORM.grid(db.evento.responsavel == auth.user_id, create=False, editable=False, searchable=False, deletable=False, csv=False, fields=[db.evento.nome_evento], paginate=1)
    rows = db(db.evento.responsavel == auth.user_id).select()
    form1 = SQLFORM(db.evento) # criar
    form2 = SQLFORM(db.evento, 2) # editar
    #rows2 = db(db.evento.id == 2).delete() #excluir


    return locals()

@auth.requires_login()
def naoaprovados():
    if auth.user.perfil == 'aprovador':
        db.evento.status.writable = True
        db.evento.destaque.writable = True

        #rows = db(db.eventos.status == 'Cadastrado')

        #form = SQLFORM.grid(db.evento, ado', create=False, editable=True, searchable=False, deletable=False, csv=False, fields=[db.evento.nome_evento], paginate=20)
        #if request.vars.status:
        #    response.flash ='achou'
        rows = db(db.evento.status == 'Cadastrado').select()
        #form1 = SQLFORM(db.evento) # criar
        if request.args(0):
            form = SQLFORM(db.evento, request.args(0)) # editar

            if form.process().accepted:

                if request.vars.status == 'Aprovado' or request.vars.status == 'Rejeitado':
                    evento = db.evento[request.args(0)]
                    r = mail_avaliacao(evento,request.vars)
                    response.flash = r
            elif form.errors:
                response.flash = 'Foram encontrados erros. Favor verificar os campos abaixo'
            else:
                response.flash = 'Favor preencher o formulário'
        else:
            form = ''
        #rows2 = db(db.evento.id == 2).delete() #excluir


    return locals()

@auth.requires_login()
def aprovados():
    if auth.user.perfil == 'aprovador':
        db.evento.status.writable = True
        db.evento.destaque.writable = True
        form = SQLFORM.grid(db.evento.status == 'Aprovado', create=False, editable=True, searchable=False, deletable=False, csv=False, fields=[db.evento.nome_evento], paginate=20, maxtextlength=200)

        #rows = db(db.evento.aprovado == None).select()
        #form1 = SQLFORM(db.evento) # criar
        #form2 = SQLFORM(db.evento, 2) # editar
        #rows2 = db(db.evento.id == 2).delete() #excluir


    return locals()

@auth.requires_login()
def rejeitados():
    if auth.user.perfil == 'aprovador':
        db.evento.status.writable = True
        db.evento.destaque.writable = True
        form = SQLFORM.grid(db.evento.status == 'Rejeitado', create=False, editable=True, searchable=False, deletable=False, csv=False, fields=[db.evento.nome_evento], paginate=20, maxtextlength=200)

        #rows = db(db.evento.aprovado == None).select()
        #form1 = SQLFORM(db.evento) # criar
        #form2 = SQLFORM(db.evento, 2) # editar
        #rows2 = db(db.evento.id == 2).delete() #excluir


    return locals()

@auth.requires_login()
def destaque():
    if auth.user.perfil == 'aprovador':
        db.evento.status.writable = True
        db.evento.destaque.writable = True
        form = SQLFORM.grid(db.evento.destaque == True, create=False, editable=True, searchable=False, deletable=False, csv=True, fields=[db.evento.nome_evento], paginate=20, maxtextlength=200)

        #rows = db(db.evento.aprovado == None).select()
        #form1 = SQLFORM(db.evento) # criar
        #form2 = SQLFORM(db.evento, 2) # editar
        #rows2 = db(db.evento.id == 2).delete() #excluir


    return locals()

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    if request.args(0) == 'profile':
        db.auth_user.cpf.writable = False
        db.auth_user.first_name.writable = False


    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
